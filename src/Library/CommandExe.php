<?php

namespace Jakmall\Recruitment\Calculator\Library;

use Jakmall\Recruitment\Calculator\Calculation\Calculator;
use Jakmall\Recruitment\Calculator\Calculation\Operators\Addition;

class CommandExe
{
	public static function run(string $command, string $operator, array $arguments, $class, $getDescription = true): string
	{
		$calculator = new Calculator($operator);
        $calculator->setCommand($command);
    	$calculator->setValues($arguments);
		$calculator->setOperation($class);
		$result = ($getDescription) ? $calculator->process()["description"] : $calculator->process()["result"];
		return $result;
	}
}