<?php

namespace Jakmall\Recruitment\Calculator\Library;

class Response
{
	public function json($data)
	{
		header('Content-Type: application/json');
		$data = json_encode($data);
		echo $data;
	}
}