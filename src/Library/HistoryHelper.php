<?php

namespace Jakmall\Recruitment\Calculator\Library;

use Jakmall\Recruitment\Calculator\Library\Helper;

class HistoryHelper
{
    public static function getInput($value)
    {
        $description = trim($value->description);
        if(strpos($description, "+")) {
            $inputs = explode("+", $description);
        }
        elseif(strpos($description, "-")) {
            $inputs = explode("-", $description);
        }
        elseif(strpos($description, "*")) {
            $inputs = explode("*", $description);
        }
        elseif(strpos($description, "/")) {
            $inputs = explode("/", $description);
        }
        elseif(strpos($description, "^")) {
            $inputs = explode("^", $description);
        }

        return $inputs;
    }

    public static function getData($id = null): array
    {
        $helper = new Helper();
        $response = [];
        $inputs = [];
        $results = json_decode($helper->getFromDatabase([], true, $id));
        if($id) {
            $inputs = self::getInput($results);
            $response = [
                "id" => $results->id,
                "command" => ucwords($results->command),
                "operation" => $results->description,
                "input" => $inputs,
                "result" => $results->result,
                "time" => $results->created_at,
            ];
        }
        else {
            foreach ($results as $key => $value) {
                $inputs = self::getInput($value);
                $response[] = [
                    "id" => $value->id,
                    "command" => ucwords($value->command),
                    "operation" => $value->description,
                    "input" => $inputs,
                    "result" => $value->result,
                    "time" => $value->created_at,
                ];
            }
        }

        return $response;
    }

    public static function deleteData($id)
    {
        $helper = new Helper();
        $helper->delete($id);
    }
}