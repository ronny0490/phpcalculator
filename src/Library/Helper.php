<?php

namespace Jakmall\Recruitment\Calculator\Library;

use Classes\CalculatorModel;

class Helper
{
	public function getFromFile(array $arguments = []): string
	{
		$path = "storage/file.txt";
	    $file = trim(file_get_contents($path));
	    $files = ($file) ? explode("\n", $file) : [];
	    $fileTemps = [];
	    $arguments = array_map('strtolower', $arguments);

	    foreach ($files as $result) {
	    	$fileResult = json_decode($result);
	        if(count($arguments) > 0) {
	        	if(in_array(strtolower($fileResult->command), $arguments)) {
	        		$fileTemps[] = $fileResult;
	        	}
	        }
	        else {
	        	$fileTemps[] = $fileResult;
	        }
	    }

	    return $this->results($fileTemps);
	}

	public function getFromDatabase(array $arguments = [], $arrayResult = false, $id = null): string
	{
		if(count($arguments) > 0) {
			$calculators = CalculatorModel::whereIn('command', $arguments)->get();
		}
		else if($id) {
			$calculators = CalculatorModel::find($id);
		}
		else {
			$calculators = CalculatorModel::get();
		}
		return (!$arrayResult) ? $this->results($calculators) : $calculators;
	}

	public function results($calculators)
	{
		$text = '';
		$number = 1;
		foreach ($calculators as $value) {
			$outputResult = $value->description.' = '.$value->result;
			$numberRepeat = 3 - (int)((String)strlen($number));
	        $commandRepeat = 9 - (int)strlen($value->command);
	        $descriptionRepeat = 17 - (int)strlen($value->description);
	        $resultRepeat = 7 - (int)strlen($value->result);
	        $outputRepeat = 26 - (int)strlen($outputResult);
	        $timeRepeat = 26 - (int)strlen($value->created_at);

	        $text .= '| '.$number.str_repeat(' ', $numberRepeat).'| '.ucwords($value->command).str_repeat(' ', $commandRepeat).'| '.$value->description.str_repeat(' ', $descriptionRepeat).'| '.$value->result.str_repeat(' ', $resultRepeat).'| '.$outputResult.str_repeat(' ', $outputRepeat).'| '.$value->created_at.str_repeat(' ', $timeRepeat).'|';
	        if($number < count($calculators)) {
	        	$text .= PHP_EOL;
	        }
	        $number++;
		}

		return $text;
	}

	public function delete($id)
	{
		CalculatorModel::find($id)->delete();
	}
}