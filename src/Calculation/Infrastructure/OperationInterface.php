<?php

namespace Jakmall\Recruitment\Calculator\Calculation\Infrastructure;

interface OperationInterface
{
    public function evaluate(array $values = array()): string;
}