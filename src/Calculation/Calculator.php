<?php

namespace Jakmall\Recruitment\Calculator\Calculation;

use Classes\CalculatorModel;
use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\OperationInterface;
use Illuminate\Database\Capsule\Manager as Capsule;

class Calculator {
	protected $values = [];
	protected $operator;
    protected $command;
	protected $results;

	public function __construct($operator)
	{
		$this->operator = $operator;
    }

    public function setCommand(string $command)
    {
        $this->command = $command;
    }

	public function setValues(array $values = array())
	{
		$this->values = $values;
	}

	public function addValue($value)
	{
		$this->values[] = $value;
	}

	public function setOperation(OperationInterface $operation)
    {
        $this->operation = $operation;
    }

    public function process()
    {
    	$total = count($this->values);
        $result = $this->operation->evaluate($this->values);
    	$counter = 1;
    	foreach($this->values as $value)
    	{
    		$this->results .= $value." ";
    		if($counter < $total)
    			$this->results .= $this->operator." ";
    		$counter++;
    	}

        $array = [
            'command' => strtolower($this->command),
            'description' => $this->results,
            'result' => $result,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        file_put_contents("storage/file.txt", json_encode($array).PHP_EOL, FILE_APPEND | LOCK_EX);
        CalculatorModel::create($array);
        return ["description" => $this->results.'= '.$result. PHP_EOL, "result" => $result];
    }
}