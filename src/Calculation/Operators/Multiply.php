<?php

namespace Jakmall\Recruitment\Calculator\Calculation\Operators;

use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\OperationInterface;

class Multiply implements OperationInterface
{
	public function evaluate(array $values = array()): string
    {
    	$results = $values[0];
    	for ($i = 1; $i < count($values); $i++) { 
    		$results *= $values[$i];
    	}

    	return $results;
    }
}