<?php

namespace Jakmall\Recruitment\Calculator\Calculation\Operators;

use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\OperationInterface;

class Addition implements OperationInterface
{
	public function evaluate(array $values = array()): string
    {
    	return array_sum($values);
    }
}