<?php

namespace Jakmall\Recruitment\Calculator\Calculation\Operators;

use Jakmall\Recruitment\Calculator\Calculation\Infrastructure\OperationInterface;

class Pow implements OperationInterface
{
	public function evaluate(array $values = array()): string
    {
    	return pow($values[0], $values[1]);
    }
}