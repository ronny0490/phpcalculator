<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Calculation\Operators\Addition;
use Jakmall\Recruitment\Calculator\Library\CommandExe;
use Jakmall\Recruitment\Calculator\Library\Response;

class CalculatorController extends Response
{
    public function calculate(Request $request)
    {
    	return $this->json([
    		"command" => $request->action,
    		"operation" => implode(" + ", $request->input),
    		"result" => CommandExe::run("add", "+", $request->input, new Addition(), false),
    	]);
    }
}
