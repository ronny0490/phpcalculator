<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Library\HistoryHelper;
use Jakmall\Recruitment\Calculator\Library\Response;

class HistoryController extends Response
{
    public function index()
    {
        $response = HistoryHelper::getData();
        return $this->json($response);
    }

    public function show(Request $request)
    {
        $response = HistoryHelper::getData($request->id);
        return $this->json($response);
    }

    public function remove(Request $request)
    {
        HistoryHelper::deleteData($request->id);
        return $this->json([]);
    }
}
