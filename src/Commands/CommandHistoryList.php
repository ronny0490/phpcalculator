<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Library\Helper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CommandHistoryList extends Command
{
	protected function configure()
    {
        $this->ignoreValidationErrors();

        $this
            ->setName('history:list')
            ->setDefinition([
                new InputArgument('commands', InputArgument::IS_ARRAY, 'Filter the history by commands'),
                new InputOption('--driver', '-D', InputOption::VALUE_OPTIONAL, 'Driver for storage connection [default: "database"]'),
            ])
            ->setDescription('Show calculator history');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = new Helper();
        $result = null;
        switch ($input->getOption('driver')) {
            case 'file':
                $result = $helper->getFromFile($input->getArgument('commands'));
                break;
            default:
                $result = $helper->getFromDatabase($input->getArgument('commands'));
                break;
        }

        if($result) {
            $output->writeln('+----+----------+------------------+--------+---------------------------+---------------------------+');
            $output->writeln('| No | Command  | Description      | Result | Output                    | Time                      |');
            $output->writeln('+----+----------+------------------+--------+---------------------------+---------------------------+');
            $output->writeln($result);
            $output->writeln('+----+----------+------------------+--------+---------------------------+---------------------------+');
        }
        else {
            $output->writeln('History is empty');
        }
    }
}