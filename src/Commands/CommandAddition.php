<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Calculation\Operators\Addition;
use Jakmall\Recruitment\Calculator\Library\CommandExe;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandAddition extends Command
{
	protected function configure()
    {
        $this->ignoreValidationErrors();

        $this
            ->setName('add')
            ->setDefinition([
                new InputArgument('numbers', InputArgument::IS_ARRAY, 'The numbers to be added'),
            ])
            ->setDescription('Add all given Numbers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$output->write(CommandExe::run("add", "+", $input->getArguments('add')['numbers'], new Addition()));
    }
}