<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Calculation\Operators\Multiply;
use Jakmall\Recruitment\Calculator\Library\CommandExe;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandMultiple extends Command
{
	protected function configure()
    {
        $this->ignoreValidationErrors();

        $this
            ->setName('multiply')
            ->setDefinition([
                new InputArgument('numbers', InputArgument::IS_ARRAY, 'The numbers to be multiplied'),
            ])
            ->setDescription('Multiply all given Numbers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write(CommandExe::run("multiply", "*", $input->getArguments('multiply')['numbers'], new Multiply()));
    }
}