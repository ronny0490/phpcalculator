<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Calculation\Operators\Pow;
use Jakmall\Recruitment\Calculator\Library\CommandExe;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandPow extends Command
{
	protected function configure()
    {
        $this->ignoreValidationErrors();

        $this
            ->setName('pow')
            ->setDefinition([
                new InputArgument('base', InputArgument::REQUIRED, 'The base number'),
                new InputArgument('exp', InputArgument::REQUIRED, 'The exponent number'),
            ])
            ->setDescription('Exponent the given Numbers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $option = $input->getArguments('pow');
        $arrays = [$option["base"], $option["exp"]];
    	$output->write(CommandExe::run("pow", "^", $arrays, new Pow()));
    }	
}