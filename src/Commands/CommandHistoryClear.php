<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Classes\CalculatorModel;
use Jakmall\Recruitment\Calculator\History\CommandHistoryServiceProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandHistoryClear extends Command
{
	protected function configure()
    {
        $this->ignoreValidationErrors();

        $this
            ->setName('history:clear')
            ->setDescription('Clear saved history');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$path = "storage/file.txt";
    	file_put_contents($path, "");
    	CalculatorModel::truncate();
    	$output->writeln('History cleared!');
    }
}