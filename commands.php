<?php

return [
    Jakmall\Recruitment\Calculator\Commands\CommandAddition::class,
    Jakmall\Recruitment\Calculator\Commands\CommandDivide::class,
	Jakmall\Recruitment\Calculator\Commands\CommandMultiple::class,
	Jakmall\Recruitment\Calculator\Commands\CommandPow::class,
	Jakmall\Recruitment\Calculator\Commands\CommandSubtract::class,
	Jakmall\Recruitment\Calculator\Commands\CommandHistoryClear::class,
	Jakmall\Recruitment\Calculator\Commands\CommandHistoryList::class,
];