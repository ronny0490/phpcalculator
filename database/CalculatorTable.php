<?php

require "../vendor/autoload.php";
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('calculators', function ($table) {
	$table->increments('id');
	$table->string('command', 15);
	$table->text('description');
	$table->integer('result');
	$table->timestamps();
});