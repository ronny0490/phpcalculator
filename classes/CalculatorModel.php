<?php

namespace Classes;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CalculatorModel extends Eloquent
{
    protected $fillable = [
        'command', 'description', 'result'
    ];
    protected $dates = ['created_at'];
    protected $table = 'calculators';
}